package org.example

import com.amazon.deequ.{VerificationResult, VerificationSuite}
import com.amazon.deequ.checks.{Check, CheckLevel, CheckStatus}
import com.amazon.deequ.constraints.ConstraintStatus
import com.amazon.deequ.examples.Item
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, StructField, StructType}
import org.apache.spark.{SparkConf, SparkContext}


object Deequ {
  def main(args: Array[String]): Unit = {

    val sc = new SparkContext(new SparkConf().setAppName("DeequProgram").setMaster("local"))
    val spark: SparkSession = SparkSession.builder.master("local").getOrCreate
    processData(sc, spark)
    sc.stop()
  }

  def processData(sc: SparkContext, spark: SparkSession): Unit = {


    val rdd = getRdd(sc)

    val df = spark.createDataFrame(rdd).toDF("id", "productName", "description", "priority", "numViews")
    df.show()

    val results: VerificationResult = getDeequResults(df)

    analizeResults(results)
  }

  def getRdd(sc: SparkContext) = {
    sc.parallelize(Seq(
      (1, "Thingy A", "awesome thing.", "high", 0),
      (2, "Thingy B", "available at http://thingb.com", null, 0),
      (3, null, null, "low", 5),
      (4, "Thingy D", "checkout https://thingd.ca", "low", 10),
      (5, "Thingy E", null, "high", 12)))
  }

  def getDeequResults(df: DataFrame): VerificationResult = {
    VerificationSuite()
      .onData(df)
      .addCheck(
        Check(CheckLevel.Error, "unit testing my data")
          .hasSize(_ == 5) // we expect 5 rows
          .isComplete("id") // should never be NULL
          .isUnique("id") // should not contain duplicates
          .isComplete("productName") // should never be NULL
          // should only contain the values "high" and "low"
          .isContainedIn("priority", Array("high", "low"))
          .isNonNegative("numViews") // should not contain negative values
          // at least half of the descriptions should contain a url
          .containsURL("description", _ >= 0.5)
          // half of the items should have less than 10 views
          .hasApproxQuantile("numViews", 0.5, _ <= 10)
          )
      .run()
  }

  def analizeResults(results: VerificationResult): Unit = {
    if (results.status == CheckStatus.Success) {
      println("The data passed the test, everything is fine!")
    } else {
      println("We found errors in the data:\n")

      val resultsForAllConstraints = results.checkResults
        .flatMap { case (_, checkResult) => checkResult.constraintResults }

      resultsForAllConstraints
        .filter { _.status != ConstraintStatus.Success }
        .foreach { result => println(s"${result.constraint}: ${result.message.get}") }
    }
  }


  def getSchema(): Unit = {
    new StructType()
    .add(StructField("id", IntegerType, true))
    .add(StructField("val1", StringType, true))
    .add(StructField("val2", StringType, true))
    .add(StructField("val3", StringType, true))
    .add(StructField("val4", IntegerType, true))
  }

}
